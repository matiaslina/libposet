module Posets where

-- Sinonimo de relaciones.
-- Dada una tupla (x,y), se dira que
-- x ~ y :)
type Relations a = [(a,a)]

-- Constructor del poset. 
-- Pendiente: Ver si hay que agregar algun constructor o algo.
data Poset a = Poset [a] (a -> a -> Bool) 

-- Elementos del poset
elems :: Poset a -> [a]
elems (Poset xs _) = xs

-- Funcion comparadora de orden del poset
compareFunction :: Poset a -> (a -> a -> Bool)
compareFunction (Poset _ f) = f

-- Consigue todas las relaciones de orden con respecto a 
-- un poset pasado por parametro. Estas relaciones dan por sentado
-- los siguientes tres puntos:
--   * Reflexividad
--   * Transitividad
--   * Antisimetria
relations :: (Ord a, Eq a) => (Poset a) -> Relations a
relations p = 
    filterByTransitivity $ filterByReflexivity $ rawRelations p 

rawRelations :: (Ord a, Eq a) => Poset a -> Relations a
rawRelations (Poset xs f) = [ (x,y) | x <- xs, y <- [ z | z <- filter (f x) xs]]

-- Funcion auxiliar para filtrar la transitividad de una lista de 
-- relaciones.
-- Basicamente lo que hace (cosa de que no se me olvide es:
--   * concatena el primer elemento de la lista
--   * hace una lista (firsts) que tiene todos los elementos que
--     se relacionan con el primer elemento del primer elemento de 
--     la lista X_x En otras palabras, si el primero de la lista es (1,2)
--     la lista que se forma seria algo como:
--              [(1,2),(1,3),(1,4)...(1,n)]        (1)
--   * luego, se hace una lista en donde los elementos de la misma
--     son aquellos elementos que tienen por primer item al segundo
--     elemento de la lista original.
--              [(2,4), (2,6),...]                 (2)
--   * Se filtra por transitividad la primer lista con respecto de la 
--     segunda.
--              [(1,2),(1,3),(1,5)...]             (3)
--   * Y se concatena a los elementos de la lista original, pero
--     desde que empieza del siguiente numero, quedando algo como
--              [(1,2),(1,3),(1,5),(2,4),(2,6)...] (4)
--   * y a (3) se le aplica la funcion filterByTransitivity de nuevo.
filterByTransitivity :: (Ord a, Eq a) => Relations a -> Relations a
filterByTransitivity [] = []
filterByTransitivity (rel:rels) = rel : (filterByTransitivity newList)
    where
        newList = tail ([ x | x <- firsts, notElem (snd x) seconds ]) ++
                  drop (length firsts) (rel:rels)
        firsts = [ x | x <- (rel:rels), fst x == fst rel ]
        seconds = [ snd x | x <- (rel:rels), fst x == snd rel ]
        len = length firsts

filterByReflexivity :: (Ord a, Eq a) => Relations a -> Relations a
filterByReflexivity rels = [ x | x <- rels, fst x /= snd x ]

-- Devuelve el maximo entre dos elementos
maximum :: (Ord a,Eq a) => Poset a -> a -> a -> Maybe a
maximum poset e1 e2 = 
    -- Chequeamos que e1 y e2 sean elementos del conjunto
    if notElem e1 (elems poset) || notElem e2 (elems poset) 
        then Nothing
        else
            if null nums 
                then Nothing 
                else Just (head nums)
    where
    nums = [ snd n | n <- e1Relations, elem (e2,snd n) e2Relations ]
    e1Relations = [ rel | rel <- rawRelations poset, fst rel == e1 ]
    e2Relations = [ rel | rel <- rawRelations poset, fst rel == e2 ]

-- Cotas superiores de *DOS* elementos. despues genrealizare a n
upperBounds :: (Ord a, Eq a) => Poset a -> a -> a -> Maybe [a]
upperBounds poset e1 e2 =
    if notElem e1 (elems poset) || notElem e2 (elems poset) 
        then Nothing
        else Just uppers
    where
    uppers = [ snd n | n <- e1Relations, elem (e2,snd n) e2Relations ]
    e1Relations = [ rel | rel <- rawRelations poset, fst rel == e1 ]
    e2Relations = [ rel | rel <- rawRelations poset, fst rel == e2 ]
            
-- Devuelve el minimo entre dos elementos
minimum :: (Ord a, Eq a) => Poset a -> a -> a -> Maybe a
minimum poset e1 e2 =
    if notElem e1 (elems poset) || notElem e2 (elems poset)
        then Nothing
        else
            if null mins
                then Nothing
                else Just (last mins)
    where
    mins = [ fst n | n <- e1Relations, elem (fst n, e2) e2Relations ]
    e1Relations = [ rel | rel <- rawRelations poset, snd rel == e1 ]
    e2Relations = [ rel | rel <- rawRelations poset, snd rel == e2 ]

-- Cotas inferiores de *DOS* elementos. despues genrealizare a n
lowerBounds :: (Ord a, Eq a) => Poset a -> a -> a -> Maybe [a]
lowerBounds poset e1 e2 =
    if notElem e1 (elems poset) || notElem e2 (elems poset)
        then Nothing
        else Just lowers
    where
    lowers = [ fst n | n <- e1Relations, elem (fst n, e2) e2Relations ]
    e1Relations = [ rel | rel <- rawRelations poset, snd rel == e1 ]
    e2Relations = [ rel | rel <- rawRelations poset, snd rel == e2 ]
--------------------------------
-- Funciones, cosas de testeo --
--------------------------------
divideA a b = b `rem` a == 0

posetDivideA :: Poset Int
posetDivideA = Poset [1,2,3,4,6,12] (divideA)

-- esto no funciona ./ toda la mala onda....
union :: (Eq a) =>  [a] -> [a] -> Bool
union [] [] = True
union [] ys = True
union xs [] = False
union (x:xs) ys = elem x ys && union xs ys
