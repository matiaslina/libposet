FILES = main.hs \
		reticulados.hs
CC=ghc
EXEC=graph

all: $(FILES)
	$(CC) $(FILES) -o $(EXEC)

clean:
	@echo "Cleaning files"
	@rm *.hi *.o $(EXEC)
