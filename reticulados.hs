module Reticulados where

import Data.List
import Data.List.Utils
import Posets


isLattice' :: (Eq a, Ord a) =>  Poset a -> Int -> Bool
isLattice' poset 0 = and $ map (/= (Just n) ) 
                         $ map (Posets.maximum poset n)
                         $ [ x | x <- elems poset, x /= (elems poset)!!0] 
    where
        n = (head (elems poset))
isLattice' poset n = (or $ map (/= (Just el) ) 
                         $ map (Posets.maximum poset el)
                         $ [ x | x <- elems poset, x /= (elems poset)!!n])
                     && (isLattice' poset (n-1))
    where
        el = (elems poset)!!n

isLattice :: (Eq a, Ord a) =>  Poset a -> Bool
isLattice poset = isLattice' poset ((length (elems poset))-1)

-----------------------------
-- Creacion de partes de X --
-----------------------------

partsOf :: [a] -> [[a]]
partsOf xs = partsOf' [[]] xs

--------------------------------------
-- Creacion de poset de partes de X --
--------------------------------------

-- Crea todas las relaciones de un poset de partes de X
posetPartsOf :: (Eq a) => [a] -> [([a],[a])]
posetPartsOf conj = [(x,y) | x <- px, y <- allExists x (px)]
    where px = partsOf conj

-- Limpia todas las relaciones a las que unicamente se cubren
-- Se usa el tamaño de los subconjuntos para eso.
reducePoset :: [([a], [a])] -> [([a],[a])]
reducePoset [] = []
reducePoset (rel:rels) = reducedRel ++ reducePoset rels
    where reducedRel = if (length (fst rel) +1) == (length (snd rel))
                       then [rel]
                       else []

-------------
-- (Dn, |) --
-------------

-- funcion para conseguir todos los divisores (Gracias stackoverflow ^^)
divisors :: Int -> [Int]
divisors n = tail [x | x <- [1..n], n `rem` x == 0]

factorize n | n > 1 = go n (primes n)
    where
        go n ds@(d:t)
            | d*d > n    = [n]
            | r == 0     =  d : go q ds
            | otherwise =      go n t
                where 
                    (q,r) = quotRem n d

factorizeDivisors :: Int -> [[Int]]
factorizeDivisors n = map factorize $ divisors n

-- Calculo de numeros primos de un numero.
primes :: Int -> [Int]
primes 1 = [1]
primes n = sieve [2..n] where
    sieve [] = []
    sieve (p:xs) = p: sieve [ x | x <- xs, x `mod` p > 0]

-- Conseguimos todas las relaciones de Dn 
dn' :: [Int] -> [(Int,Int)]
dn' [] = []
dn' (x:xs) = [ (m,n) | m <- divisors x, n <- [x]] ++ dn' xs

dn :: Int -> [(Int,Int)]
dn n = dn' (divisors n)

-- Ahora nos quedamos con las relaciones que nos interesan tratando de 
-- que quede mas bonito asi se puede dibujar
posetDn :: Int -> [([Int],[Int])]
posetDn n = [(x,y) | x <- facts, y<- allExists x facts]
    where
        facts = []:factorizeDivisors n
-- --------------------------------
-- Funciones para pasar a string --
-----------------------------------

listToString :: [String] -> String
listToString [] = ""
listToString (x:[]) = x
listToString (x:xs) = x ++ "," ++ listToString xs


elemToString :: String -> String -> ([String],[String]) -> String
elemToString open close (l1,l2) = "\"" ++
    open ++ 
    (listToString l2) ++
    close ++ "\" -- \"" ++ open ++
    (listToString l1) ++
    close ++ "\""


posetPXToString :: [([String],[String])] -> String
posetPXToString [] = ""
posetPXToString (x:[]) = elemToString "{" "}" x
posetPXToString (x:xs) = elemToString "{" "}" x ++ "\n" ++ posetPXToString xs

posetToString :: [([String],[String])] -> String
posetToString [] = ""
posetToString (x:[]) = elemToString "" "" x
posetToString (x:xs) = elemToString "" "" x ++ "\n" ++ posetToString xs

listShowToString :: Show a => [a] -> String
listShowToString [] = ""
listShowToString (x:[]) = show x
listShowToString (x:xs) = (show x) ++ "," ++ listShowToString xs

elemIntToString :: ([Int],[Int]) -> String
elemIntToString (l1,l2) = "\"" ++ (show $ product l2) ++ 
                          "\" -- \"" ++ (show $ product l1) ++ 
                          "\"" 

dnToString :: [([Int],[Int])] -> String
dnToString [] = ""
dnToString (e:[]) = elemIntToString e
dnToString (e:poset) = elemIntToString e ++ "\n" ++ dnToString poset

--------------------------
-- Funciones auxiliares --
--------------------------

-- funcion recursiva de partes de X
partsOf' :: [[a]] -> [a] -> [[a]]
partsOf' pre [] = pre
partsOf' pre [x] = pre ++ appendToAll [x] pre
partsOf' pre (x:xs) = partsOf' (partsOf' pre [x]) xs

-- Agrega una lista a todos los elementos de la lista
-- de listas
-- Ej:
--  appendTo "c" ["a","ba", "cas"] -> ["ac","bac","casc"]
appendToAll :: [a] -> [[a]] -> [[a]]
appendToAll a xs = map (++a) xs 

allExists :: (Eq a) => [a] -> [[a]] -> [[a]]
allExists xs [] = []
allExists xs (ys:yss)   | isInsideOf xs ys = ys : allExists xs yss
                        | otherwise = allExists xs yss

isInsideOf :: (Eq a) => [a] -> [a] -> Bool
isInsideOf [] xs = True
isInsideOf xs [] = False
isInsideOf (x:xs) (y:ys) = ((x==y) || isInsideOf (x:xs) ys) && 
                            isInsideOf xs (y:ys)


-----------------------------------
-- Funciones para testeo de P(x) --
-----------------------------------
partsOfa :: [[String]]
partsOfa = partsOf ["a"]

partsOfab :: [[String]]
partsOfab = partsOf ["a","b"]

partsOfabc :: [[String]]
partsOfabc = partsOf ["a","b","c"]
