module Main 
    where

import System.Environment
import System.IO
import Reticulados

main = do
    args <- getArgs
    case (head args) of
        "-D" -> do
            let poset = reverse $ reducePoset $ posetDn $ (read (last args) :: Int)
            putStr "graph px {\n"
            putStr (dnToString poset)
            putStr "\n}"
        "-P" -> do
            let px = reverse $ reducePoset $ posetPartsOf args
            putStr "graph px {\n"
            putStr (posetPXToString px)
            putStr "\n}"
        _ -> do
            let poset = parsePosetFromArgs $ args
            putStr "graph px {\n"
            putStr (posetToString poset)
            putStr "\n}"
            
parsePosetFromArgs :: [String] -> [([String],[String])]
parsePosetFromArgs [] = []
parsePosetFromArgs (x:[]) = []
parsePosetFromArgs (x:y:[]) = [([x],[y])]
parsePosetFromArgs (x:y:xs) = [([x],[y])] ++ parsePosetFromArgs xs
